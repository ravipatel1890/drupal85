<?php

namespace Drupal\address\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface as StorageDefinition;

class Multidate extends FieldItemBase 
{

	public static function propertyDefinitions(StorageDefinition $storage) {

    $properties = [];

   	$properties['multitdate'] = DataDefinition::create('varchar')
      ->setLabel(t('Multidate'));
 

    return $properties;
  }

   public static function schema(StorageDefinition $storage) {

    $columns = [];
    $columns['multitdate'] = [
      'type' => 'char',
      'length' => 255,
    ]; 
    return [
      'columns' => $columns,
      'indexes' => [],
    ];
  }
  public function isEmpty() {

    $isEmpty = 
      empty($this->get('multitdate')->getValue());
    return $isEmpty;
  }

}
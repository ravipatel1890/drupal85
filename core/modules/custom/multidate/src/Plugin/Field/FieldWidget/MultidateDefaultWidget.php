<?php

namespace Drupal\address\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

class MultidateDefaultWidget extends WidgetBase {


	 public function formElement(
    FieldItemListInterface $items,
    $delta, 
    Array $element, 
    Array &$form, 
    FormStateInterface $formState
  )  {

	 	$element['multidate'] = [
      '#type' => 'textfield',
      '#title' => t('Date'),
      '#default_value' => isset($items[$delta]->multidate) ? 
          $items[$delta]->multidate : null,
      '#empty_value' => '',
      '#placeholder' => t('Date'),
    ];
     return $element;
    }
}